<?php

function twitter_bootstrap_website_theme_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'user_register_form') {
  	$form['actions']['submit']['#attributes']['class'][] = "btn-primary";
    drupal_set_title(t('Create new account'));
  }
  elseif ($form_id == 'user_pass') {
  	$form['actions']['submit']['#attributes']['class'][] = "btn-primary";
    drupal_set_title(t('Request new password'));
  }
  elseif ($form_id == 'user_login') {
  	$form['actions']['submit']['#attributes']['class'][] = "btn-primary";
  	drupal_set_title(t('Log in'));
  }
}

/**
 * Implements theme_file().
 */
function twitter_bootstrap_website_theme_file($variables) {
  _form_set_class($variables['element'], array('input-file'));
  $variables['element']['#attributes']['size'] = '22';
  return theme_file($variables);
}

function twitter_bootstrap_website_theme_status_messages(&$variables) {
  $display = $variables['display'];
  $output = '';

  $message_info = array(
    'status' => array(
      'heading' => 'Status message',
      'class' => 'success',
    ),
    'error' => array(
      'heading' => 'Error message',
      'class' => 'error',
    ),
    'warning' => array(
      'heading' => 'Warning message',
      'class' => '',
    ),
  );
  
  foreach (drupal_get_messages($display) as $type => $messages) {
    $message_class = $type != 'warning' ? $message_info[$type]['class'] : 'warning';
    $output .= "<div class=\"alert alert-block alert-$message_class fade in\">\n<a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>";
    if (!empty($message_info[$type]['heading'])) {
      $output .= '<h2 class="element-invisible">' . $message_info[$type]['heading'] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * Implements theme_button().
 */
function twitter_bootstrap_website_theme_button($variables) {
  $variables['element']['#attributes']['class'][] = 'btn';
  return theme_button($variables);
}

/**
 * Implements theme_checkbox().
 */
function twitter_bootstrap_website_theme_checkbox($variables) {
  $element = $variables['element'];
  $t = get_t();
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array('id', 'name', '#return_value' => 'value'));

  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));

  switch ($element['#title_display']) {
    case 'attribute':
      $element['#attributes']['#title'] = $element['#description'];
      $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
      break;
    case 'before':
      $output = '<label class="checkbox">' . $element['#description'] . '<input' . drupal_attributes($element['#attributes']) . ' /></label>';
      break;
    case 'invisible':
    case 'after':
    default:

      $checkbox = '<input' . drupal_attributes($element['#attributes']) . ' />';
      if (isset($element['#description'])) {
        $output = '<label class="checkbox">' . $checkbox . $element['#description'] . '</label>';
      }
      else {
        $output = $checkbox;
      }
      break;
  }

  return $output;
}


/**
 * Implements theme_checkboxes().
 */
function twitter_bootstrap_website_theme_checkboxes($variables) {
  $option_children = '';
  foreach ($variables['element']['#options'] as $key => $description) {
    $option_variables = array(
      'element' => $variables['element'][$key],
    );
    $option_variables['element']['#description'] = $option_variables['element']['#title'];
    $option_children .= theme('checkbox', $option_variables) . "\n";
  }
  $variables['element']['#children'] = $option_children;
  $element = $variables['element'];
  return !empty($element['#children']) ? $element['#children'] : '';
}


/**
 * Implements theme_fieldset().
 */
function twitter_bootstrap_website_theme_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }
  $output .= '<div class="fieldset-wrapper row-fluid">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;  
}

/**
 * Implements theme_radio():
 */
function twitter_bootstrap_website_theme_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array('id', 'name', '#return_value' => 'value'));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  switch ($element['#title_display']) {
    case 'attribute':
      $element['#attributes']['#title'] = $element['#description'];
      $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
      break;
    case 'before':
      $output = '<label class="radio">' . $element['#description'] . '<input' . drupal_attributes($element['#attributes']) . ' /></label>';
      break;
    case 'invisible':
    case 'after':
    default:
      $radio = '<input' . drupal_attributes($element['#attributes']) . ' />';
      if (isset($element['#description'])) {
        $output = '<label class="radio">' . $radio . $element['#description'] . '</label>';
      }
      else {
        $output = $radio;
      }
      break;
  }

  return $output;
}

/**
 * Implements theme_radios().
 */
function twitter_bootstrap_website_theme_radios($variables) {
  $option_children = '';
  foreach ($variables['element']['#options'] as $key => $description) {
    $option_variables = array(
      'element' => $variables['element'][$key],
    );
    $option_variables['element']['#description'] = $option_variables['element']['#title'];
    $option_children .= theme('radio', $option_variables) . "\n";
  }
  $variables['element']['#children'] = $option_children;
  $element = $variables['element'];
  return !empty($element['#children']) ? $element['#children'] : '';
}

/**
 * Implements theme_textarea().
 */
function twitter_bootstrap_website_theme_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('input-xlarge'));

  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }
  else {
    $wrapper_attributes = array();
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}

function twitter_bootstrap_website_theme_form_element_label($variables) {
  $element = $variables['element'];
  $t = get_t();

  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  if (isset($attributes['class'])) {
    $attributes['class'] .= ' control-label';
  }
  else {
    $attributes['class'] = 'control-label';
  }

  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

function twitter_bootstrap_website_theme_css_alter(&$css) {
    unset($css[drupal_get_path('module','system').'/system.theme.css']);
    unset($css[drupal_get_path('module','system').'/system.menus.css']);
    unset($css[drupal_get_path('module','system').'/system.base.css']);
}

function twitter_bootstrap_website_theme_menu_tree__navigation($variables) {
    $output = '<ul class="menu nav nav-list bs-docs-sidenav affix-top">' . $variables['tree'] . '</ul>';
    return $output;
}

function twitter_bootstrap_website_theme_menu_link__navigation(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $linktext = '<a href="/' . $element['#href'] . '"><i class="icon-chevron-right"></i>' . $element['#title'] . '</a>';
  //$output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $output = $linktext;
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function twitter_bootstrap_website_theme_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary nav nav-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary nav nav-pills">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
* Preprocesses the wrapping HTML.
*/
function twitter_bootstrap_website_theme_preprocess_html(&$vars) {
 
  $bootstrap_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    )
  );
 
  drupal_add_html_head($bootstrap_viewport, 'bootstrap_viewport');
}

function twitter_bootstrap_website_theme_preprocess_node(&$vars, $hook) {
  $vars['submitted'] = t('@date', array('@date' => date("l, M jS, Y", $vars['created'])));
}

function twitter_bootstrap_website_theme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
      $crumbs = '<ul class="nav">';

      foreach($breadcrumb as $value) {
           $crumbs .= '<li>'.$value.'</li>';
      }

      $crumbs .= '<li class="active"><a href="#" title"'.drupal_get_title().'">'.drupal_get_title().'</a></li>';
      $crumbs .= '</ul>';

      return $crumbs;
    }
  }